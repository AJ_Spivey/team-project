# Returns the sum of two numbers.
from phcpy.solver import solve
from phcpy.solutions import filter_real,strsol2dict
import sys
#from phcpy.factor import solve
def solve_system(pols):
    # Solve a system
    s = solve(pols, verbose=False)
    realsols = filter_real(s, 1.0e-8, 'select')
    for solution in realsols:
        d = strsol2dict(solution)
        realsols = [str(complex(d[var]).real) for var in ['x','y','z']]
        print(', '.join(realsols))

pols = [str(arg) for arg in sys.argv[1:]]
solve_system(pols)