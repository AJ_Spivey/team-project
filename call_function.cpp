#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <vector>
#include <sstream>
#include <math.h>
using namespace std;

#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

string get_current_dir()
{
    char buff[FILENAME_MAX]; //create string buffer to hold path
    GetCurrentDir(buff, FILENAME_MAX);
    string current_working_dir(buff);
    return current_working_dir;
}

string exec(const char *cmd)
{
    array<char, 128 * 16> buffer;
    string result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe)
    {
        throw runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
    {
        result += buffer.data();
    }
    return result;
}

void tokenize(string const &str, const char delim,
              vector<std::string> &out)
{
    // construct a stream from the string
    stringstream ss(str);

    string s;
    while (getline(ss, s, delim))
    {
        out.push_back(s);
    }
}

vector<vector<double>> getSolutions(vector<string> polys)
{
    // the command we want to run (to do python stuffs)
    string currentDir = get_current_dir();
    ostringstream dirStream;
    dirStream << "cd " << currentDir << "/PHCpack-master/src/Python/PHCpy2";
    // string gotoDirectory = "cd /mnt/c/Users/spive/Desktop/classwork/parallelProcessing/groupProject/PHCpack-master/src/Python/PHCpy2";
    string gotoDirectory = dirStream.str();
    string runScript = "python solve_system.py";
    string commandString = "";

    // build the command
    commandString.append(gotoDirectory);
    commandString.append("; ");
    commandString.append(runScript);
    commandString.append(" ");
    // Add the polynomials we want to solve (as arguments to python)
    for (auto &poly : polys)
    {
        commandString.append("\'");
        commandString.append(poly);
        commandString.append(";\' ");
    }

    // Convert the command string to a const char*
    const char *command = commandString.c_str();

    // The stdout output of our command
    string result = exec(command);

    // First line is hello message -- get rid of it
    result.erase(0, result.find("\n") + 1);

    // The next n lines are each a real solution to our system.
    vector<string> solutionStrings;
    vector<vector<double>> solutions;
    tokenize(result, '\n', solutionStrings);

    // Turn the lines into double solutions of x,y,z
    for (auto &solutionString : solutionStrings)
    {
        // For each solution, split it into x, y, and z.
        vector<string> xyz;
        vector<double> solution;
        tokenize(solutionString, ',', xyz);
        // For each xyz, cast to double and add to solution
        for (auto &elem : xyz)
        {
            solution.push_back(stod(elem));
        }
        solutions.push_back(solution);
    }

    return solutions;
}
