#include <iostream>
#include <math.h>
#include <list>
#include <iterator>
#include <array>
#include <vector>
#include <utility>
#include <random>
using namespace std;

#define ROW 4
#define COL 5

class PowerSets
{

public:
    vector<vector<int>> payoffs;
    int players, actions;
    PowerSets(int playersIn, int actionsIn)
    {
        players = playersIn;
        actions = actionsIn;
        // generate the right amount of random numbers
        int columnCount = pow(actions,players);
        random_device rd; // obtain a random number from hardware
        mt19937 gen(rd()); // seed the generator
        uniform_int_distribution<> distr(1, 20); // define the range

        
        for (int player = 0; player < players; player++){
            vector<int> playerPayoffs;
            for (int i = 0; i < columnCount; i++){
                playerPayoffs.push_back(distr(gen));
            }
            payoffs.push_back(playerPayoffs);
        }
        payoffs.push_back({9, 4, 6, 3, 1, 7, 2, 8});
        payoffs.push_back({1, 5, 2, 4, 3, 8, 2, 9});
        payoffs.push_back({7, 1, 7, 2, 6, 3, 5, 1});
    }
    // void printPowerSet(string set[], int set_size)
    vector<vector<string>> getPowerSet(string set[])
    {
        int set_size = getActions();
        /*set_size of power set of a set with set_size 
    n is (2**n -1)*/
        unsigned int pow_set_size = pow(2, set_size);
        int counter, j;
        // vector<vector<string>> powerset[pow_set_size];
        vector<vector<string>> powerset;

        // list<string> a;
        // string x;
        /*Run from counter 000..0 to 111..1*/
        for (counter = 0; counter < pow_set_size; counter++)
        {
            vector<string> curr;
            for (j = 0; j < set_size; j++)
            {
                /* Check if jth bit in the counter is set 
            If set then print jth element from set */
                if (counter & (1 << j))
                {
                    curr.push_back(set[j]);
                }
            }
            powerset.push_back(curr);
        }
        return powerset;
    }

    vector<vector<vector<string>>> getPowerSetFromActions()
    {
        string set[actions];
        for (int i = 1; i <= actions; i++)
        {
            set[i - 1] = to_string(i);
        }
        vector<vector<string>> powersets = getPowerSet(set);
        return getCombinationOfPowersets(powersets, powersets, powersets);
    }

    vector<vector<vector<string>>> getCombinationOfPowersets(vector<vector<string>> set1, vector<vector<string>> set2, vector<vector<string>> set3)
    {
        vector<vector<vector<string>>> comb;
        for (vector<string> a : set1)
        {
            if (a.size() < 2)
                continue;
            for (vector<string> b : set2)
            {
                if (b.size() < 2)
                    continue;
                for (vector<string> c : set3)
                {
                    if (c.size() < 2)
                        continue;
                    vector<vector<string>> temp;
                    temp.push_back(a);
                    temp.push_back(b);
                    temp.push_back(c);
                    comb.push_back(temp);
                }
            }
        }
        return comb;
    }

    double getUtilities(int i, int j, vector<vector<double>> sigma)
    {

        // rho[0][0] = 9 * 0.3 * 0.2 + 4 * 0.3 * 0.8 + 6 * 0.7 * 0.2 + 3 * 0.7 * 0.8 = 4.02;
        // rho[1][0] = 1 * 0.4 * 0.2 + 5 * 0.4 * 0.8 + 3 * 0.6 * 0.2 + 8 * 0.6 * 0.8 = 6.5;
        double rho = 0;

        int offset = pow(actions, i);
        vector<int> vals;
        int ind = j * offset;
        while (ind < payoffs[i].size())
        {
            for (int x = 0; x < offset; x++)
            {
                vals.push_back(payoffs[i][ind + x]);
            }
            ind += offset * 2;
        }
        int counter = 0;
        for (int a2 = 0; a2 < actions; a2++)
        {
            for (int a1 = 0; a1 < actions; a1++)
            {
                //int payoff = payoffs[i][actions * (players - 1) * j + (a1 * actions) + a2];
                // int payoff = payoffs[i][offset * a1 + a2];
                int payoff = vals[counter];
                counter++;
                // cout << "payoff " << payoff << endl;
                rho += payoff * sigma[0][a1] * sigma[1][a2];
            }
        }
        return rho;
        // rho[0][0] = 9 * 0.3 * 0.2 + 4 * 0.3 * 0.8 + 6 * 0.7 * 0.2 + 3 * 0.7 * 0.8 = 4.02;
        // rho[0][1] = 1 * 0.3 * 0.2 + 7 * 0.3 * 0.8 + 2 * 0.7 * 0.2 + 8 * 0.7 * 0.8 = 6.5;
    }

    pair<vector<vector<int>>, vector<vector<int>>> getSupportPair(vector<vector<double>> sigma)
    {
        vector<vector<int>> support;
        vector<vector<int>> not_support;
        pair<vector<vector<int>>, vector<vector<int>>> output;
        for (auto item : sigma)
        {
            vector<int> s;
            vector<int> not_s;
            for (int i = 0; i < item.size(); i++)
            {
                if (item[i] == 0)
                {
                    not_s.push_back(i);
                }
                else
                {
                    s.push_back(i);
                }
            }
            support.push_back(s);
            not_support.push_back(not_s);
        }
        output.first = support;
        output.second = not_support;
        return output;
    }

    vector<vector<vector<double>>> getSigmas()
    {
        vector<vector<vector<double>>> sigmas;
        vector<vector<double>> sigma1;
        sigma1.push_back({0.5, 0.5});
        sigma1.push_back({0.75, 0.25});
        sigma1.push_back({0.57, 0.43});

        vector<vector<double>> sigma2;
        sigma2.push_back({0.875, 0.125});
        sigma2.push_back({1.5, 0});
        sigma2.push_back({0.5, 0.5});

        sigmas.push_back(sigma1);
        sigmas.push_back(sigma2);
        return sigmas;
    }

    vector<vector<double>> getRhos(vector<vector<double>> sigma)
    {
        vector<vector<double>>
            rhos;
        for (int i = 0; i < players; i++)
        {
            vector<vector<double>> new_sigma;
            for (int x = 0; x < players; x++)
            {
                if (i == x)
                    continue;
                new_sigma.push_back(sigma[x]);
            }
            vector<double> rho;
            for (int j = 0; j < actions; j++)
            {
                double utility = getUtilities(i, j, new_sigma);
                // cout << "utility " << utility << endl;
                rho.push_back(utility);
                // cout << g.getUtilities(i, j, new_sigma) << endl;
            }
            rhos.push_back(rho);
        }
        return rhos;
    }

    void replaceAll( string &s, const string &search, const string &replace ) {
        for( size_t pos = 0; ; pos += replace.length() ) {
            // Locate the substring to replace
            pos = s.find( search, pos );
            if( pos == string::npos ) break;
            // Replace by erasing and inserting
            s.erase( pos, search.length() );
            s.insert( pos, replace );
        }
    }

    vector<string> buildPolys()
    {
        vector<string> polys;

        // For each player
        for (int i = 0; i < players; i++)
        {
            // We're gonna build a polynomial for each player
            string poly;
            ostringstream polystream;
            // one iteration of this is adding, the other is subtracting
            for (int j = 0; j < actions; j++)
            {
                vector<int> vals;
                int offset = pow(actions, i);
                int ind = j * offset;
                while (ind < payoffs[i].size())
                {
                    for (int x = 0; x < offset; x++)
                    {
                        vals.push_back(payoffs[i][ind + x]);
                    }
                    ind += offset * 2;
                }

                int counter = 0;

                // For each of the other player's actions, build the polynomial
                if (j != 0) {
                    polystream << " -(";
                }
                for (int a2 = 0; a2 < actions; a2++)
                {
                    for (int a1 = 0; a1 < actions; a1++)
                    {
                        int payoff = vals[counter];
                        counter++;
                        polystream << payoff;
                        polystream << "*";
                        switch (i)
                        {
                        case 0:
                            polystream << "y" << a1 << "*z" << a2;
                            if (a1 != actions-1 || a2 != actions-1){
                                polystream << " + ";
                            }
                            break;
                        case 1:
                            polystream << "x" << a1 << "*z" << a2;
                            if (a1 != actions-1 || a2 != actions-1){
                                polystream << " + ";
                            }
                            break;
                        case 2:
                            polystream << "x" << a1 << "*y" << a2;
                            if (a1 != actions-1 || a2 != actions-1){
                                polystream << " + ";
                            }
                            break;
                        }
                    }
                }
                if (j != 0) {
                    polystream << ")";
                }
                // replace x2 with (1-x1), y2 with (1-y1), etc.
                poly = polystream.str();
                replaceAll(poly, "x0", "x");
                replaceAll(poly, "y0", "y");
                replaceAll(poly, "z0", "z");
                replaceAll(poly, "x1", "(1-x)");
                replaceAll(poly, "y1", "(1-y)");
                replaceAll(poly, "z1", "(1-z)");
            }
            polys.push_back(poly);
        }
        return polys;
    }

    void sequential()
    {
        int actions = 2;
        auto powersets = getPowerSetFromActions();
        for (auto set : powersets)
        {
        }
        int a = 1;
    }

    int getPlayers()
    {
        return players;
    }

    int getActions()
    {
        return actions;
    }
};

/*Driver code*/
/*
int main()
{
    PowerSets g;
    // g.getPowerSetFromActions(3);
    g.sequential();
    int actions = 2;
    int players = 3;
    vector<vector<double>> sigma;
    sigma.push_back({0.5, 0.5});
    sigma.push_back({0.75, 0.25});
    sigma.push_back({0.57, 0.43});

    pair<vector<vector<int>>, vector<vector<int>>> support_pair = g.getSupportPair(sigma);

    int i = 0;
}
*/