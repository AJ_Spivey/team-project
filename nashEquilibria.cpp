#include <iostream>
#include <math.h>
#include <list>
#include <iterator>
#include <array>
#include <vector>
#include <utility>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <sstream>
#include <chrono>

#include "powersets.cpp"
#include "call_function.cpp"

using namespace std;

/*Driver code*/
void sequentialAlgorithm(PowerSets g){
    int i, j;
    double eps = 1e-8;
    int players, actions;
    players = g.getPlayers();
    actions = g.getActions();
    
    /*
    Get every combination of powersets for each player
    This represents getting every combination of mixed strategies 
    that the group of players might play
    */
    auto mixedProfiles = g.getPowerSetFromActions();

    /*
    For each mixed strategy profile (set of strategies for each player),
    See if there is a nash equilibrium
    */
    for (auto &strategyProfile: mixedProfiles){
        // For each player in the profile, generate a polynomial
        vector<string> polys = g.buildPolys();

        // call the solver for the system built
        auto solutions = getSolutions(polys);
        

        // For each solution to the system, check if it is a valid nash equilibrium.
        for (auto solution: solutions){
            // Adjust the solution for the format getRhos expects
            vector<vector<double>> sigma;
            for (auto element: solution){
                vector<double> el;
                el.push_back(element);
                el.push_back(1-element);
                sigma.push_back(el);
            }
            auto rhos = g.getRhos(sigma);

            bool nash = true;
            // for each player, check if the solution represents a nash equilibrium
            for (i=0; i<players; i++){
                double psum = 0;
                for (j=0; j<actions; j++){
                    if (sigma[i][j] > 1 || sigma[i][j] <= 0){
                        nash = false;
                    }
                    // Since there are only two actions, we do not need to check S_i - support(sigma_i) (since it will always be empty)
                    psum += sigma[i][j];
                }
                if (psum > 1 + eps || psum  < 1 - eps){
                    nash = false;
                }
            }
            if (nash){
                // print out this solution
                for (auto &element: solution){
                    cout << element << ", ";
                }
                cout << endl;
            }
        }
    }
}

void parallelAlgorithm(PowerSets g){
    int i, j;
    double eps = 1e-8;
    int players, actions;
    players = g.getPlayers();
    actions = g.getActions();
    
    /*
    Get every combination of powersets for each player
    This represents getting every combination of mixed strategies 
    that the group of players might play
    */
    auto mixedProfiles = g.getPowerSetFromActions();

    /*
    For each mixed strategy profile (set of strategies for each player),
    See if there is a nash equilibrium
    */
    for (auto &strategyProfile: mixedProfiles){
        // For each player in the profile, generate a polynomial
        vector<string> polys = g.buildPolys();

        // call the solver for the system built
        auto solutions = getSolutions(polys);
        

        // For each solution to the system, check if it is a valid nash equilibrium.
        for (auto solution: solutions){
            // Adjust the solution for the format getRhos expects
            vector<vector<double>> sigma;
            for (auto element: solution){
                vector<double> el;
                el.push_back(element);
                el.push_back(1-element);
                sigma.push_back(el);
            }
            auto rhos = g.getRhos(sigma);

            int nash = 0;

            // for each player, check if the solution represents a nash equilibrium
            #pragma omp parallel for reduction(+: nash)
            for (i=0; i<players; i++){
                double psum = 0;
                for (j=0; j<actions; j++){
                    if (sigma[i][j] > 1 || sigma[i][j] <= 0){
                        nash += 1;
                    }
                    // Since there are only two actions, we do not need to check S_i - support(sigma_i) (since it will always be empty)
                    psum += sigma[i][j];
                }
                if (psum > 1 + eps || psum  < 1 - eps){
                    nash += 1;
                }
            }
            if (nash == 0){
                // print out this solution
                for (auto &element: solution){
                    cout << element << ", ";
                }
                cout << endl;
            }
        }
    }
}

int main()
{
    // Make a game
    PowerSets g(3,2);
    // Time the sequential algorithm
    auto start = chrono::high_resolution_clock::now(); 
    sequentialAlgorithm(g);
    auto stop = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::microseconds>(stop - start); 
    cout << duration.count() << endl;
    // Time the parallel algorithm
    auto start2 = chrono::high_resolution_clock::now(); 
    parallelAlgorithm(g);
    auto stop2 = chrono::high_resolution_clock::now();
    auto duration2 = chrono::duration_cast<chrono::microseconds>(stop2 - start2); 
    cout << duration2.count() << endl;
}